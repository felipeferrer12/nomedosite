+++
title= "Transfers"
date= 2023-09-26T10:28:46-03:00
draft= false
image = 'images/soccer_boy.svg'
+++

Esta página contém informações sobre as últimas transferências do mercado da bola.

1. Leroy Sané - Manchester City para Bayern de Munique: O extremo alemão Leroy Sané se transferiu do Manchester City para o Bayern de Munique em julho de 2020 por uma quantia relatada de cerca de € 49 milhões. Sua transferência fortaleceu ainda mais o ataque do Bayern.

2. Timo Werner - RB Leipzig para Chelsea: O atacante alemão Timo Werner mudou-se do RB Leipzig para o Chelsea em junho de 2020 por uma taxa de transferência de aproximadamente € 53 milhões. Sua velocidade e habilidade de finalização eram muito aguardadas pelos torcedores dos Blues.

3. Kai Havertz - Bayer Leverkusen para Chelsea: O meia-atacante alemão Kai Havertz também se juntou ao Chelsea em setembro de 2020, em uma transferência que custou cerca de € 80 milhões. Ele foi uma das contratações mais caras do verão europeu.

4. Arthur - Barcelona para Juventus: Em uma troca de jogadores notável, o meio-campista brasileiro Arthur se transferiu do Barcelona para a Juventus em junho de 2020, com Miralem Pjanic indo na direção oposta. A transferência foi avaliada em torno de € 72 milhões.

5. Nathan Aké - Bournemouth para Manchester City: O zagueiro holandês Nathan Aké se transferiu do Bournemouth para o Manchester City em agosto de 2020 por cerca de € 45 milhões, reforçando a defesa dos Citizens.

6. Achraf Hakimi - Real Madrid para Inter de Milão: O lateral marroquino Achraf Hakimi foi transferido do Real Madrid para a Inter de Milão em julho de 2020 por aproximadamente € 40 milhões. Sua versatilidade e velocidade o tornaram uma adição valiosa à equipe italiana.

7. Miralem Pjanic - Juventus para Barcelona: Como parte da troca com Arthur, o meio-campista bósnio Miralem Pjanic se mudou da Juventus para o Barcelona em agosto de 2020, com um valor relatado de € 60 milhões.

8. Hakim Ziyech - Ajax para Chelsea: O meia marroquino Hakim Ziyech transferiu-se do Ajax para o Chelsea em julho de 2020 por cerca de € 40 milhões. Sua criatividade e habilidade com a bola eram esperadas com ansiedade pelos torcedores do Chelsea.

9. Donny van de Beek - Ajax para Manchester United: O meio-campista holandês Donny van de Beek se juntou ao Manchester United em setembro de 2020 por cerca de € 39 milhões, fortalecendo o meio-campo dos Red Devils.

10. Victor Osimhen - Lille para Napoli: O atacante nigeriano Victor Osimhen fez a mudança do Lille para o Napoli em julho de 2020, em uma transferência que custou cerca de € 70 milhões. Ele era visto como um jovem talento promissor.
