+++
title = 'Juninho Pernambucano: From VASCO to LYON'
date = 2023-09-26T11:38:04-03:00
draft = false
description = 'A contratação recebeu nota 5 de 5'
price = 'Não houve custos de transferência'
image = 'images/vasco_lyon.svg'
data = '01/07/2000'
+++

A contratação do jogador Juninho Pernambucano pelo Lyon merece uma nota de 5 em 5. Abaixo, apresento uma análise para justificar essa classificação:

Pontos Positivos:

Qualidade do Jogador: Juninho Pernambucano foi um dos meio-campistas mais icônicos e talentosos da sua geração. Sua habilidade em chutar de longa distância e sua maestria nas cobranças de falta o tornaram um jogador excepcional. Ele se tornou uma lenda do Lyon, sendo fundamental para o sucesso do clube.

Longevidade e Contribuição Duradoura: Juninho passou várias temporadas no Lyon, contribuindo consistentemente com gols e assistências. Sua presença constante no time ajudou o clube a conquistar vários títulos da Ligue 1 e a se destacar na Europa.

Custo da Transferência: O preço pago pelo Lyon para adquirir Juninho foi relativamente baixo em comparação com a qualidade e a contribuição que ele trouxe ao clube. Foi um investimento altamente lucrativo para o Lyon.

Avaliação do Preço da Transferência:

O custo da transferência de Juninho Pernambucano para o Lyon pode ser considerado muito baixo, considerando o impacto que ele teve no clube. Sua capacidade de liderança, habilidades excepcionais e contribuição consistente ao longo de várias temporadas tornaram-no um dos maiores jogadores da história do Lyon. O investimento inicial na sua transferência provou ser um dos melhores negócios da história do futebol francês.

Em resumo, a contratação de Juninho pelo Lyon foi extremamente bem-sucedida e altamente lucrativa para o clube. Sua nota máxima reflete a importância e a qualidade que ele trouxe ao time durante seu tempo no clube, além do custo acessível da transferência, que aumentou ainda mais o valor dessa aquisição.
